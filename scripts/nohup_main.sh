source "${RIDERS_GLOBAL_PATH}/lib/helpers.sh"

echo -e "$GREEN"
echo " --------------------"
echo "| Proje Hazırlanıyor |"
echo " --------------------"
echo ""

ride_show_riders_commands
ride_ui_resize_panel left 0
ride_ui_resize_panel right 0
ride_ui_resize_panel bottom 150

# Setup Godot and FastApi
source "${RIDE_PACKAGES_PATH}/thor/scripts/godot_setup.sh"
source "${RIDE_PACKAGES_PATH}/thor/scripts/api_setup.sh"

# Sleep 3 seconds to wait for api_server to boot
sleep 3

# Show Simulation and Blockly in Iframe
#source "${RIDE_SOURCE_RIDERS_PATH}/commands/b_open_blockly_code.sh"
sleep 1
source "${RIDE_SOURCE_RIDERS_PATH}/commands/a_open_godot_web_export.sh"
